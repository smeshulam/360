 // JavaScript Document Made By Shahar Meshulam 2013
// Getting elements
var gameZone = document.getElementById('gameZone');
//var canvas = document.getElementById('game');
var backGround = document.getElementById('background');
var pad = document.getElementById("pad");
var ball = document.getElementById("ball");
var svg = document.getElementById("svgRoot");
var message = document.getElementById("message");
//var context = canvas.getContext("2d");
var windowHeight;// = backGround.offsetHeight;
var windowWidth;// = backGround.offsetHeight;
var middleX;
var middleY;
// Ball
var ballRadius = ball.r.baseVal.value;
var ballX;
var ballY;
var previousBallPosition = { x: 0, y: 10 };
var ballDirectionX;
var ballDirectionY;
var ballSpeed = 5;

// Pad
var padWidth = pad.width.baseVal.value;
var padHeight = pad.height.baseVal.value;
var padX;
var padY;
var padSpeed = 0;
var inertia = 0.80;
var toDeg = 180/Math.PI;
var padAngle = 0
var padCurAngle = 0;
var Radians// = Math.atan2(padX,padY);
// convert Radians degrees to Degrees
var Degrees// = Radians * 180 / Math.PI;
// rotate
//pad.rotation = Degrees;

	
// Bricks
var bricks = [];
var destroyedBricksCount;
var brickWidth = 50;
var brickHeight = 20;
var bricksRows = 6;
var bricksCols = 5;
var bricksMargin = 10;
var bricksTop = 20;
	
// Misc.
var minX = ballRadius;
var minY = ballRadius;
var maxX;
var maxY;
var startDate;

function init(){
	console.log("init");
	var dimentions = svg.getBoundingClientRect();
	windowHeight = dimentions.height;
	windowWidth = dimentions.width;
	backGround.style.width = backGround.offsetHeight + 'px';
	middleX = windowWidth/2;
	middleY = windowHeight/2;
}

// Brick function
function Brick(x, y) {
    var isDead = false;
    var position = { x: x, y: y };

    var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
    svg.appendChild(rect);

    rect.setAttribute("width", brickWidth);
    rect.setAttribute("height", brickHeight);

    // Random green color
    var chars = "456789abcdef";
    var color = "";
    for (var i = 0; i < 2; i++) {
        var rnd = Math.floor(chars.length * Math.random());
        color += chars.charAt(rnd);
    }
    rect.setAttribute("fill", "#00" + color + "00");

    this.drawAndCollide = function () {
        if (isDead)
            return;
        // Drawing
        rect.setAttribute("x", position.x);
        rect.setAttribute("y", position.y);

        // Collision
        if (ballX + ballRadius < position.x || ballX - ballRadius > position.x + brickWidth)
            return;

        if (ballY + ballRadius < position.y || ballY - ballRadius > position.y + brickHeight)
            return;

        // Dead
        this.remove();
        isDead = true;
        destroyedBricksCount++

        // Updating ball
        ballX = previousBallPosition.x;
        ballY = previousBallPosition.y;

        ballDirectionY *= -1.0;
    };

    // Killing a brick
    this.remove = function () {
        if (isDead)
            return;
        svg.removeChild(rect);
    };
}

// Collisions
function collideWithWindow() {
   /* if (ballX < minX) {
        ballX = minX;
        ballDirectionX *= -1.0;
    }
    else if (ballX > maxX) {
        ballX = maxX;
        ballDirectionX *= -1.0;
    }

    if (ballY < minY) {
        ballY = minY;
        ballDirectionY *= -1.0;
    }
    else if (ballY > maxY) {
        ballY = maxY;
        ballDirectionY *= -1.0;
        lost();
    }*/
}

function collideWithPad() {
    if (ballX + ballRadius < padX || ballX - ballRadius > padX + padWidth)
        return;

    if (ballY + ballRadius < padY)
        return;

    ballX = previousBallPosition.x;
    ballY = previousBallPosition.y;
    ballDirectionY *= -1.0;

    var dist = ballX - (padX + padWidth / 2);

    ballDirectionX = 2.0 * dist / padWidth;

    var square = Math.sqrt(ballDirectionX * ballDirectionX + ballDirectionY * ballDirectionY);
    ballDirectionX /= square;
    ballDirectionY /= square;
}

// Pad movement
function movePad() {
	//console.log(pad.rotate.baseVal.value);
	padWidth = pad.width.baseVal.value;
	padHeight = pad.height.baseVal.value;
	  padCurAngle = padAngle * Math.PI / 180; 
	  // angle of rotation in radians
      var rx = middleX, ry = middleY;
	  // the rotation x and y
      var px =  padWidth, py =  padHeight; // the objects center x and y
      var radius = ry - py; // the difference in y positions or the radius
      var dx = (rx + radius * Math.sin(padCurAngle))  // the draw x 
      var dy = (ry - radius * Math.cos(padCurAngle))  // the draw y
		pad.setAttribute("transform", "rotate(" + padAngle + " " + middleX + " " + middleY +")");
	var ballRadians = Math.atan2(middleY - ballY, middleX - ballX);
     	padX = dx;
		padY = dy;
		
		
}


window.addEventListener('keydown', function (evt) {
    switch (evt.keyCode) {
        // Left arrow
        case 37:
            padSpeed -= 10;
            break;
        // Right arrow   
        case 39:
            padSpeed += 10;
            break;
    }
}, true);

function checkWindow() {
    maxX = windowWidth - minX;
    maxY = windowHeight;// - minY;
    //padY = maxY-100;
}

function gameLoop() {
    movePad();

    // Movements
    previousBallPosition.x = ballX;
    previousBallPosition.y = ballY;
    ballX += ballDirectionX * ballSpeed;
    ballY += ballDirectionY * ballSpeed;

    // Collisions
   // collideWithWindow();
    //collideWithPad();
	console.log(hitTest(pad,ball)+"hit");

    // Bricks
    for (var index = 0; index < bricks.length; index++) {
        bricks[index].drawAndCollide();
    }

    // Ball
    ball.setAttribute("cx", ballX);
    ball.setAttribute("cy", ballY);

    // Pad
    //pad.setAttribute("x", padX);
    //pad.setAttribute("y", padY);
	//console.log(padX);
	//console.log(padY);
	//console.log(padAngle);
	//console.log(padWidth + "width");
	
	
    
    // Victory ?
    if (destroyedBricksCount == bricks.length) {
        win();
    }
}

function generateBricks() {
    // Removing previous ones
    for (var index = 0; index < bricks.length; index++) {
        bricks[index].remove();
    }

    // Creating new ones
    var brickID = 0;

    var offset = (windowWidth - bricksCols * (brickWidth + bricksMargin)) / 2.0;

    for (var x = 0; x < bricksCols; x++) {
        for (var y = 0; y < bricksRows; y++) {
            bricks[brickID++] = new Brick(offset + x * (brickWidth + bricksMargin), y * (brickHeight + bricksMargin) + bricksTop);
        }
    }
}

var gameIntervalID = -1;
function lost() {
    clearInterval(gameIntervalID);
    gameIntervalID = -1;
    
    message.innerHTML = "<p>Game over !</p><p onclick = 'startGame()'>newGame</p>";
    message.style.visibility = "visible";
}

function win() {
    clearInterval(gameIntervalID);
    gameIntervalID = -1;

    var end = (new Date).getTime();

    message.innerHTML = "Victory ! (" + Math.round((end - startDate) / 1000) + "s)";
    message.style.visibility = "visible"; 
}

function initGame() {
    message.style.visibility = "visible";
	padX = ( middleX - (padWidth/2));
    ballX = middleX;
    ballY = middleY + 80;
	padSpeed = 0;
    checkWindow();
    
    previousBallPosition.x = ballX;
    previousBallPosition.y = ballY;
    
    ballDirectionX = Math.random();
    ballDirectionY = -1.0;

    generateBricks();
    gameLoop();
}

function startGame() {
	message.innerHTML =""
	init();
    initGame();

    destroyedBricksCount = 0;

    if (gameIntervalID > -1)
        clearInterval(gameIntervalID);

    startDate = (new Date()).getTime(); ;
    gameIntervalID = setInterval(gameLoop, 16);
}


function ballHitTest( ){
	
};

/*
function rot(rx,ry,P)
{	
	var dx = m.x-rx;
	var dy = m.y-ry;
	var cos = Math.cos(P);
	var sin = Math.sin(P);

	m.rotation += P*toDeg;
	m.x = rx+dx*cos-dy*sin;
	m.y = ry+dy*cos+dx*sin;
}	*/

document.getElementById("newGame").onclick = startGame;