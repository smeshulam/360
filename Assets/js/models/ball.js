// JavaScript Document Made By Shahar Meshulam 2019

export class Ball{
	/*
	r;
	x;
	y;
	*/
	
	constructor(data = {}) {
		Object.assign(this, data);
		if(!this.speed) {
			this.speed = 0;
		}

		this.elem = document.createElementNS("http://www.w3.org/2000/svg", "polygon");
		this.elem.setAttribute('transform', `translate(${this.x}, ${this.y})`);
		this.vx = 0.3;
		this.vy = -1;
		this.previousPosition = {x: this.x, y: this.y};
		this.generatePolygon();
	}

	moveBall() {
		this.previousPosition.x = this.x;
		this.previousPosition.y = this.y;
		this.x += this.vx;
		this.y += this.vy;
		this.elem.setAttribute('transform', `translate(${this.x},${this.y})`);
	}
	
	get vector() {
		return [this.vx, this.vy];
	}
	
	// Collide handler
	collide(b) {
		// get angle
		let bx = b.x;
		if(b.width)
		bx = bx - b.width/2

		const theta = -Math.atan2(b.y - this.y, bx- this.x);
		let btheta;
		if(b.name && b.name === 'pad'){
			let c = b.elem.getAttribute('transform');
			c = c.substring(
				c.lastIndexOf("(") + 1, 
				c.lastIndexOf(")")
			);
			c = c.split(',');
			btheta = (parseFloat(c[0]) * (Math.PI / 180)) + 180 ;
			
		}

		// mass
		const m1 = this.mass,
			  m2 = this.mass
		// update vectors
		const v1 = this.rotate(this.vector, theta),
			  v2 = this.rotate([1,1], theta);
		// calculate momentum
		const u1 = this.rotate([v1[0] * (m1 - m2) / (m1 + m2) + v2[0] * 2 * m2 / (m1 + m2), v1[1]], -theta),
			  u2 = this.rotate([v2[0] * (m2 - m1) / (m1 + m2) + v1[0] * 2 * m1 / (m1 + m2), v2[1]], -theta);
		// set new velocities
		if(btheta) {
			const diffrance = (btheta - (theta-180));
			this.vx = Math.cos(diffrance);
			this.vy = Math.sin(diffrance);
		} else {
			this.vx = u1[0];
			this.vy = u1[1];
		}
		// Dispatch collide 
		this.dispatchEvent(new CustomEvent('collide', {bubbles: true, detail: {elem: b}}));
	}

	
	// Angle rotate calculation
	rotate(v, theta) {
		return [v[0] * Math.cos(theta) - v[1] * Math.sin(theta), v[0] * Math.sin(theta) + v[1] * Math.cos(theta)];
	}
  
	intersectRect(r1, r2) {
		var r1 = r1.getBoundingClientRect(); //BOUNDING BOX OF THE FIRST OBJECT
		var r2 = r2.getBoundingClientRect(); //BOUNDING BOX OF THE SECOND OBJECT
	 
		//CHECK IF THE TWO BOUNDING BOXES OVERLAP
	  	return !(r2.left > r1.right || 
			   r2.right < r1.left || 
			   r2.top > r1.bottom ||
			   r2.bottom < r1.top);
	}

	// Check if ball collide with other element (soft check)
	checkForIntercept(elem) {
		// (soft check)
		if (this.intersectRect(this.elem, elem)) {
			//persice check (check for polygon collision)
			
		}
	}

	generatePolygon() {
		const sideCount = this.r * 2;
		const radius = this.r / 2;
		const cx = this.x;
		const cy = this.y;
		const s = 2 * radius + 50;
	  
		const res = this.polygon([cx, cy], sideCount, radius);
		const viz = this.polygon([s / 2, s / 2], sideCount, radius);
		
		this.elem.setAttribute('points', viz);
	  }

	polygon([cx, cy], sideCount, radius) {
		return this.pts(sideCount, radius)
		  .map(({ r, theta }) => [
			cx + r * Math.cos(theta), 
			cy + r * Math.sin(theta),
		  ])
		  .join(' '); 
	}

	pts(sideCount, radius) {
		const angle = 360 / sideCount;
		const vertexIndices = range(sideCount);
		const offsetDeg = 90 - ((180 - angle) / 2);
		const offset = degreesToRadians(offsetDeg);
	  
		return vertexIndices.map((index) => {
		  return {
			theta: offset + degreesToRadians(angle * index),
			r: radius,
		  };
		});
		function range(count) {
			return Array.from(Array(count).keys());
		}
		function degreesToRadians(angleInDegrees) {
			return (Math.PI * angleInDegrees) / 180;
		}
	  }
}