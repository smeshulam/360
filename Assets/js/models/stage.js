// JavaScript Document Made By Shahar Meshulam 2019

export class Stage {  
  /*
  window;
	svg;
	background;
  demintions
  */
  
  constructor(data = {}) {
    Object.assign(this, data);
    this.height =  this.demintions.height;
    this.width = this.demintions.width;
    this.middleX = this.demintions.width/2;
    this.middleY = this.demintions.height/2;
	  this.mouseX = 0;
	  this.mouseY = 0;
	  this.window.onmousemove = this.handleMouseMove;
  }

  add(elem) {
    this.svg.appendChild(elem);
  }
  
  handleMouseMove(event) {
	  var eventDoc, doc, body;
    event = event || window.event; // IE-ism
    // If pageX/Y aren't available and clientX/Y are,
    // calculate pageX/Y - logic taken from jQuery.
    // (This is to support old IE)
    if (event.pageX == null && event.clientX != null) {
        eventDoc = (event.target && event.target.ownerDocument) || document;
        doc = eventDoc.documentElement;
        body = eventDoc.body;
        event.pageX = event.clientX +
          (doc && doc.scrollLeft || body && body.scrollLeft || 0) -
          (doc && doc.clientLeft || body && body.clientLeft || 0);
        event.pageY = event.clientY +
          (doc && doc.scrollTop  || body && body.scrollTop  || 0) -
          (doc && doc.clientTop  || body && body.clientTop  || 0 );
    }

    event.currentTarget.dispatchEvent(new CustomEvent('change', {bubbles: true, detail: {mouseX: event.pageX, mouseY: event.pageY }}));
   }
  
}