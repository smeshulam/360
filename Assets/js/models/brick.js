// JavaScript Document Made By Shahar Meshulam 2019

export class Brick {
	/*x;
	y;
	r;
	centerX;
	centerY;
	n;
	i;
	isAlive;
	size;
	elem;
	mass
	position = {};*/

	constructor(data = {}) {
		Object.assign(this, data);
		this.isAlive = true;
		this.position = { x: this.x, y: this.y };
		this.size = 100 / this.n;
		this.elem = document.createElementNS("http://www.w3.org/2000/svg", "path");
		this.mass = 1;
		this.elem.setAttribute('stroke', "red");
		this.elem.setAttribute('stroke-width', "1");
		this.elem.setAttribute('fill', "red");
		this.elem.setAttribute('fill-rule',"evenodd");
		this.elem.setAttribute('d', "M 150 20 A 130 130 0 1 1 149.7731073124332 20.000198001272594 M 150 50 A 100 100 0 1 1 149.82546716341017 50.00015230867123");
		this.elem.setAttribute('r', this.r);
		this.elem.setAttribute('cx', this.x);
		this.elem.setAttribute('cy', this.y);
		this.elem.setAttribute('stroke-width', 20);
		this.elem.setAttribute('stroke-dasharray', `${this.size-1}% ${100-this.size+1}%`);
		this.elem.setAttribute('stroke-dashoffset', `${(this.size * this.i)}%`);
		
		// Random green color;
		var chars = "456789abcdef";
		var color = "";
		for (var i = 0; i < 2; i++) {
			var rnd = Math.floor(chars.length * Math.random());
			color += chars.charAt(rnd);
		}
		this.elem.setAttribute('stroke', '#00cc00');
		//this.elem.setAttribute("fill", 'transparent');
  }

  draw() {
	var centerX= this.x;
	var centerY= this.y;

	var outerRadius=150;
	var innerRadius=30;
	var startAngle=0//--degrees
	var endAngle=359.9//--degrees

	function polarToCartesian(centerX, centerY,radiusX, radiusY, angleInDegrees)
	{
		var angleInRadians = (angleInDegrees-90) * Math.PI / 180.0;
		return {
		x: centerX + (radiusX * Math.cos(angleInRadians)),
		y: centerY + (radiusY * Math.sin(angleInRadians))
		};
	}
	//---outer points---
	var StartPnt1 = polarToCartesian(centerX, centerY, outerRadius, outerRadius, startAngle);
	var EndPnt1 = polarToCartesian(centerX, centerY,  outerRadius, outerRadius, endAngle);
	//---inner points---
	//var StartPnt2 = polarToCartesian(centerX, centerY, innerRadius, innerRadius, startAngle);
	//var EndPnt2 = polarToCartesian(centerX, centerY,  innerRadius, innerRadius, endAngle);

	//---use segList object--
	//--path fill-rule="evenodd" ---
	var mySegList=this.elem.pathSegList
	mySegList.clear()

	var pathSegM1=this.elem.createSVGPathSegMovetoAbs(StartPnt1.x,StartPnt1.y)
	var pathSegA1=this.elem.createSVGPathSegArcAbs(EndPnt1.x,EndPnt1.y,outerRadius,outerRadius,0,1,1)
	//var pathSegM2=this.elem.createSVGPathSegMovetoAbs(StartPnt2.x,StartPnt2.y)
	//var pathSegA2=this.elem.createSVGPathSegArcAbs(EndPnt2.x,EndPnt2.y,innerRadius,innerRadius,0,1,1)
	mySegList.appendItem(pathSegM1)
	mySegList.appendItem(pathSegA1)
	//mySegList.appendItem(pathSegM2)
	//mySegList.appendItem(pathSegA2)
}
  
  remove = function () {
	if (this.isAlive)
		return;
        //stage.svg.removeChild(this.elem);
  };
}

   