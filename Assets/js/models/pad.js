// JavaScript Document Made By Shahar Meshulam 2019

export class Pad {
	/*elem;	
	name;
	x;
	y;
	width;
	height;
	speed;
	angle;
	mass;*/
	
  constructor(data = {}) {
      Object.assign(this, data);
	  if(!this.speed) {
			this.name = 'pad';
			this.speed = 0;
		}

		this.mass= 1;
		this.elem.setAttribute('x', this.x);
		this.elem.setAttribute('y', this.y);
  }
  
  setAngle() {
		elem.setAttribute('transform', `rotate(45) ${this.angle}`);
  }
}