// JavaScript Document Made by Shahar meshulam 2013
// Getting elements
/*var pad = document.getElementById("pad");
var ball = document.getElementById("ball");
var svg = document.getElementById("svgRoot");
var message = document.getElementById("message");

// Ball
var ballRadius = ball.r.baseVal.value;
var ballX;
var ballY;
var previousBallPosition = { x: 0, y: 0 };
var ballDirectionX;
var ballDirectionY;
var ballSpeed = 10;

// Pad
var padWidth = pad.width.baseVal.value;
var padHeight = pad.height.baseVal.value;
var padX;
var padY;
var padSpeed = 0;
var inertia = 0.80;

// Bricks
var bricks = [];
var destroyedBricksCount;
var brickWidth = 50;
var brickHeight = 20;
var bricksRows = 5;
var bricksCols = 20;
var bricksMargin = 15;
var bricksTop = 20;

// Misc.
var minX = ballRadius;
var minY = ballRadius;
var maxX;
var maxY;
var startDate;
*/

var gLoop,
	c = document.getElementById('Game'), 
	ctx = c.getContext('2d');
	
var toDeg = 180/Math.PI;
function rot(rx,ry,P)
{	
	var dx = m.x-rx;
	var dy = m.y-ry;
	var cos = Math.cos(P);
	var sin = Math.sin(P);

	m.rotation += P*toDeg;
	m.x = rx+dx*cos-dy*sin;
	m.y = ry+dy*cos+dx*sin;
}	

var clear = function(){
	ctx.fillStyle = '#d0e7f9';
	ctx.clearRect(0, 0, c.width, c.height);
	ctx.beginPath();
	ctx.rect(0, 0,c.width, c.height);
	ctx.closePath();
	ctx.fill();
	alert("!!!");
	var stats = new Stats();
	stats.setMode( 1 );
	stats.begin();
	document.body.appendChild( stats.domElement );
	var context = c.getContext( '2d' );
	context.fillStyle = 'rgba(127,0,255,0.05)';
	setInterval( function () {
		stats.begin();
		stats.end();
	}, 1000 / 60 );
}

var Brick = function(x, y, type){  
	var isDead = false;
	var position = { x: x, y: y };
	switch(type){
		case 0:
			this.firstColor = '#FF8C00';  
			this.secondColor = '#EEEE00';  
			this.onCollide = function(){  
				player.fallStop();  
			}  
		case 1:
			this.firstColor = '#AADD00';  
			this.secondColor = '#698B22';  
			this.onCollide = function(){  
				player.fallStop();  
				player.jumpSpeed = 50;  
			};  
	}  
	  
	var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
    svg.appendChild(rect);
    rect.setAttribute("width", brickWidth);
    rect.setAttribute("height", brickHeight);
	rect.setAttribute("fill", "url(#green)");
	this.type = type;  
	  
	this.draw = function(){
		ctx.fillStyle = 'rgba(255, 255, 255, 1)';
		var gradient = ctx.createRadialGradient(that.x + (platformWidth/2), that.y + (platformHeight/2), 5, that.x + (platformWidth/2), that.y + (platformHeight/2), 45);
		gradient.addColorStop(0, that.firstColor);
		gradient.addColorStop(1, that.secondColor);
		ctx.fillStyle = gradient;
		ctx.fillRect(that.x, that.y, platformWidth, platformHeight);
	};  
	  
	return this;  
};  

/////////////////////////////////////////////////////////
var checkCollision = function(){
	platforms.forEach(function(e, ind){
	//check every plaftorm
	if (
	(player.X < e.x + platformWidth) && 
	(player.X + player.width > e.x) && 
	(player.Y + player.height > e.y) && 
	(player.Y + player.height < e.y + platformHeight)
	//and is directly over the platform
	) {
	e.onCollide();
	}
	})
}

/////////////////////////////////////////////////////////
/*var nrOfPlatforms = 20,   
platforms = [],  
platformWidth = 20,  
platformHeight = 10;  
var generatePlatforms = function(){
	var position = 0, type;
	//'position' is Y of the platform, to place it in quite similar intervals it starts from 0
	for (var i = 0; i < nrOfPlatforms; i++) {
	type = ~~(Math.random()*5);
	if (type == 0) type = 1;
	else type = 0;
	//it's 5 times more possible to get 'ordinary' platform than 'super' one
	platforms[i] = new Platform(Math.random()*(width-platformWidth),position,type);
	//random X position
	if (position < height - platformHeight) 
		position += ~~(height / nrOfPlatforms);
	}
	//and Y position interval
}();
//we call that function only once, before game start
checkCollision(); */ 