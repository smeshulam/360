 // JavaScript Document Made By Shahar Meshulam 2019
import { Stage } from "./models/stage.js";
import { Pad } from "./models/pad.js"; 
import { Ball } from "./models/ball.js";
import { Brick } from './models/brick.js';

// Getting elements
// Stage
const demintions = svgRoot.getBoundingClientRect();
const stage = new Stage({'window': window.document, 'svg': document.getElementById("svgRoot"), 'background':document.getElementById('background'), 'demintions': demintions});

// Pad
const padElem = document.getElementById("pad");
const pad = new Pad({'elem': padElem, 'width': padElem.width.baseVal.value, 'height': padElem.height.baseVal.value, 'speed': 0, 'x': stage.middleX - padElem.width.baseVal.value/2, 'y':stage.height -  padElem.height.baseVal.value})

// Message
const message = document.getElementById("message");

// Ball;
const ball = new Ball({'x': stage.middleX, 'y': pad.height + 10, 'r': 10});

// Define vars
let gameIntervalID = -1;
let listenersAttached = false;

const bricks = [];
let destroyedBricksCount = 0;
let brickWidth = 3;
let startDate;

// Init game
const init = () => {
    console.log("init");
    stage.svg.appendChild(ball.elem);
	if(!listenersAttached) {
		attachListeners()
	}
}

// Attach Game Listeners
function attachListeners() {
	stage.window.addEventListener('change', movePad);
    
	listenersAttached = true;
}

// Game loop functions handle
function gameLoop() {
    moveBall();
    handleBallCollideWithBricks()
    //handleBallCollideWithPad();
}

// Handle collision with pad
function handleBallCollideWithPad() {
    ball.checkForIntercept(pad);
}

// Handle collision with all bricks 
function handleBallCollideWithBricks() {
    ball.checkForIntercept(bricks[0]);
}

// Pad movement
function movePad(event) {
	const angleDeg = Math.atan2( event.detail.mouseY - stage.middleY, event.detail.mouseX - stage.middleX) * 180 / Math.PI ;
    pad.elem.setAttribute("transform", `rotate(${angleDeg-90}, ${stage.middleX}, ${stage.middleY})`);
}

// Handles ball movment  
function moveBall() {
    ball.moveBall();
}

// Generate Bricks line
function generateBricks() {
    // Removing previous ones
    /*for (var index = 0; index < bricks.length; index++) {
        bricks[index].remove();
    }*/

    // Creating new ones
    //for (var x = 0; x < 10; x++) {
       const segment = new Brick({  'x': stage.middleX,
									'y': stage.middleY,
									'r': 150,
									'n': 10,
                                    //'i': x,
								});
	   
       stage.svg.appendChild(segment.elem);
       bricks.push(segment);
       segment.draw();
    //}
}

//TODO
function lost() {
    clearInterval(gameIntervalID);
    gameIntervalID = -1;
    message.innerHTML = "<p>Game over !</p><p onclick = 'startGame()'>newGame</p>";
    message.style.visibility = "visible";
}

//TODO
function win() {
    clearInterval(gameIntervalID);
    gameIntervalID = -1;
    const end = (new Date).getTime();
    message.innerHTML = "Victory ! (" + Math.round((end - startDate) / 1000) + "s)";
    message.style.visibility = "visible"; 
}


// initialize game elements to start position 
function initGame() {
    message.style.visibility = "visible";
    pad.x = stage.middleX - pad.width / 2;
    pad.y = stage.height - pad.height;

    ball.r = 20;
    ball.x = stage.middleX - pad.width / 2;
    ball.y = pad.y - ball.y - ball.r;
    // Generate ball polygon 
    ball.generatePolygon();
	//pad.speed = 0;
    
    ////ball.previousPosition.x = ball.x;
    //ball.previousPosition.y = ball.y;
    
    ball.vx = Math.random();
    ball.vy = -1.0;

    generateBricks();
    gameLoop();
}


// Handle start game
function startGame() {
	message.innerHTML =""
	init();
    initGame();

    destroyedBricksCount = 0;

    if (gameIntervalID > -1)
        clearInterval(gameIntervalID);

    startDate = (new Date()).getTime(); ;
    gameIntervalID = setInterval(gameLoop, 16);
}

document.getElementById("newGame").onclick = startGame;
init();

//fps engine 
/*
var box = document.getElementById('box'),
    fpsDisplay = document.getElementById('fpsDisplay'),
    boxPos = 10,
    lastBoxPos = 10,
    boxVelocity = 0.08,
    limit = 300,
    lastFrameTimeMs = 0,
    maxFPS = 60,
    delta = 0,
    timestep = 1000 / 60,
    fps = 60,
    framesThisSecond = 0,
    lastFpsUpdate = 0;

function update(delta) {
    boxLastPos = boxPos;
    boxPos += boxVelocity * delta;
    // Switch directions if we go too far
    if (boxPos >= limit || boxPos <= 0) boxVelocity = -boxVelocity;
}

function draw(interp) {
    box.style.left = (boxLastPos + (boxPos - boxLastPos) * interp) + 'px';
    fpsDisplay.textContent = Math.round(fps) + ' FPS';
}

function panic() {
    delta = 0;
}

function begin() {
}

function end(fps) {
    if (fps < 25) {
        box.style.backgroundColor = 'black';
    }
    else if (fps > 30) {
        box.style.backgroundColor = 'red';
    }
}

function mainLoop(timestamp) {
    // Throttle the frame rate.    
    if (timestamp < lastFrameTimeMs + (1000 / maxFPS)) {
        requestAnimationFrame(mainLoop);
        return;
    }
    delta += timestamp - lastFrameTimeMs;
    lastFrameTimeMs = timestamp;

    begin(timestamp, delta);

    if (timestamp > lastFpsUpdate + 1000) {
        fps = 0.25 * framesThisSecond + 0.75 * fps;

        lastFpsUpdate = timestamp;
        framesThisSecond = 0;
    }
    framesThisSecond++;

    var numUpdateSteps = 0;
    while (delta >= timestep) {
        update(timestep);
        delta -= timestep;
        if (++numUpdateSteps >= 240) {
            panic();
            break;
        }
    }

    draw(delta / timestep);

    end(fps);

    requestAnimationFrame(mainLoop);
}

requestAnimationFrame(mainLoop);
*/